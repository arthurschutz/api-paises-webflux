package com.forttiori.configuration.integration.service;

import com.forttiori.configuration.integration.client.PaisClient;
import com.forttiori.configuration.integration.response.PaisInfoResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Flux;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PaisServiceIntegrationTest {

    @Mock
    PaisClient paisClient;

    @InjectMocks
    PaisServiceIntegration paisServiceIntegration;

    PaisInfoResponse paisInfoResponse = PaisInfoResponse.builder()
            .name("Brasil")
            .capital("Brasília")
            .region("América")
            .build();

    List<PaisInfoResponse> listaPaises = new ArrayList<>();
    Flux<PaisInfoResponse> paisInfoResponseFlux = Flux.fromIterable(listaPaises);

    @Test
    void deveRetornarOPaisPeloNome() {

        listaPaises.add(paisInfoResponse);

        when(paisClient.getPaisByName(paisInfoResponse.getName())).thenReturn(paisInfoResponseFlux);

        var response = paisServiceIntegration.getPaisByName(paisInfoResponse.getName());
        var stubExpect = paisInfoResponseFlux;

        assertEquals(stubExpect, response);
    }

    @Test
    void deveRetornarOPaisPelaCapital() {

        listaPaises.add(paisInfoResponse);

        when(paisClient.getPaisByCapital(paisInfoResponse.getCapital())).thenReturn(paisInfoResponseFlux);

        var response = paisServiceIntegration.getPaisByCapital(paisInfoResponse.getCapital());
        var stubExpect = paisInfoResponseFlux;

        assertEquals(stubExpect, response);
    }

    @Test
    void deveRetornarOPaisPelaRegiao() {

        listaPaises.add(paisInfoResponse);

        when(paisClient.getPaisByRegiao(paisInfoResponse.getRegion())).thenReturn(paisInfoResponseFlux);

        var response = paisServiceIntegration.getPaisByRegiao(paisInfoResponse.getRegion());
        var stubExpect = paisInfoResponseFlux;

        assertEquals(stubExpect, response);
    }

    @Test
    void deveRetornarTodosOsPaises() {

        listaPaises.add(paisInfoResponse);

        when(paisClient.getTodosPaises()).thenReturn(paisInfoResponseFlux);

        var response = paisServiceIntegration.getTodosPaises();
        var stubExpect = paisInfoResponseFlux;

        assertEquals(stubExpect, response);
    }
}