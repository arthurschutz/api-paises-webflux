package com.forttiori.configuration.integration.client;

import com.forttiori.configuration.integration.response.PaisInfoResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import reactor.core.publisher.Flux;


@Component("PaisClient")
@FeignClient(name = "pais-client", url = "https://restcountries.eu/rest/v2/")
public interface PaisClient {

    @GetMapping("capital/{capital}")
    Flux<PaisInfoResponse> getPaisByCapital(@PathVariable("capital") String capital);

    @GetMapping("name/{name}")
    Flux<PaisInfoResponse> getPaisByName(@PathVariable("name") String name);

    @GetMapping("region/{region}")
    Flux<PaisInfoResponse> getPaisByRegiao(@PathVariable("region") String region);

    @GetMapping
    Flux<PaisInfoResponse> getTodosPaises();
}
