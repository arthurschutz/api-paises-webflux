package com.forttiori.configuration.integration.service;

import com.forttiori.configuration.integration.client.PaisClient;
import com.forttiori.configuration.integration.response.PaisInfoResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;


@Service
@AllArgsConstructor
public class PaisServiceIntegration {

    private final PaisClient paisClient;

    public Flux<PaisInfoResponse> getPaisByName(String name) {
        return this.paisClient.getPaisByName(name);
    }

    public Flux<PaisInfoResponse> getPaisByCapital(String capital) {
        return this.paisClient.getPaisByCapital(capital);
    }

    public Flux<PaisInfoResponse> getPaisByRegiao(String region) {
        return this.paisClient.getPaisByRegiao(region);
    }

    public Flux<PaisInfoResponse> getTodosPaises() {
        return this.paisClient.getTodosPaises();
    }

}
