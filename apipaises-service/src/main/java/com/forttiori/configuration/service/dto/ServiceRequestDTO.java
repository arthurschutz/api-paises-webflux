package com.forttiori.configuration.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ServiceRequestDTO {

    @ApiModelProperty(value = "Nome do país")
    private String name;

    @ApiModelProperty(value = "Região do país")
    private String region;

    @ApiModelProperty(value = "Capital do país")
    private String capital;
}
