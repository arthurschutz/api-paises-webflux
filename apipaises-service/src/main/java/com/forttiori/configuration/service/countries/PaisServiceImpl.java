package com.forttiori.configuration.service.countries;

import com.forttiori.configuration.integration.response.PaisInfoResponse;
import com.forttiori.configuration.integration.service.PaisServiceIntegration;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;

import java.util.List;

@AllArgsConstructor
@Service
public class PaisServiceImpl implements PaisService{

    private final PaisServiceIntegration paisServiceIntegration;

    @Override
    public Flux<PaisInfoResponse> getPaisByCapital(String capital) {
        try{
            return this.paisServiceIntegration.getPaisByCapital(capital);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND ,"CAPITAL NÃO ENCONTRADA!");
        }
    }

    @Override
    public Flux<PaisInfoResponse> getPaisByName(String name) {
        try {
            return this.paisServiceIntegration.getPaisByName(name);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "PAÍS NÃO ENCONTRADO!");
        }
    }

    @Override
    public Flux<PaisInfoResponse> getPaisByRegiao(String region) {
        try {
            return this.paisServiceIntegration.getPaisByRegiao(region);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND ,"REGIÃO NÃO ENCONTRADA!");
        }
    }

    @Override
    public Flux<PaisInfoResponse> getTodosPaises() {
        return this.paisServiceIntegration.getTodosPaises();
    }
}
