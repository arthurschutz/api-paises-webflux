package com.forttiori.configuration.service.countries;

import com.forttiori.configuration.integration.response.PaisInfoResponse;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.util.List;

@Service
public interface PaisService {

    Flux<PaisInfoResponse> getPaisByCapital(String capital);
    Flux<PaisInfoResponse> getPaisByName(String name);
    Flux<PaisInfoResponse> getPaisByRegiao(String region);
    Flux<PaisInfoResponse> getTodosPaises();
}
