package com.forttiori.configuration.service.reservations;

import com.forttiori.configuration.entity.Reservation;
import com.forttiori.configuration.persistence.repository.ReservationRepository;
import com.forttiori.configuration.service.dto.MapperService;
import com.forttiori.configuration.service.dto.ServiceRequestDTO;
import com.forttiori.configuration.service.dto.ServiceResponseDTO;
import lombok.AllArgsConstructor;
import org.reactivestreams.Publisher;
import org.springframework.data.domain.Example;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;


@Service
@AllArgsConstructor
public class ReservationServiceImpl implements ReservationService {

    private final ReservationRepository reservationRepository;

    @Override
    public Mono<ServiceResponseDTO> save(ServiceRequestDTO serviceRequestDTO) {
        return Mono.justOrEmpty(serviceRequestDTO)
                .map(request -> MapperService.mapToReservationEntity(serviceRequestDTO))
                .flatMap(reservationRepository::save)
                .map(MapperService::mapToServiceResponse);

    }
    @Override
    public Flux<Reservation> findAll() {
        return reservationRepository.findAll()
                .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NO_CONTENT, "Banco de dados está vazio")));
    }

    @Override
    public Mono<Reservation> findByIdEntity (String id) {
        return Mono.just(id)
                .flatMap(reservationRepository::findById)
                .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND, "Reserva não encontrada!")));
    }

    @Override
    public Mono<ServiceResponseDTO> findByIDResponse(String id) {
        return findByIdEntity(id).map(MapperService::mapToServiceResponse);
    }

    @Override
    public Mono<ServiceResponseDTO> update(String id, ServiceRequestDTO serviceRequestDTO) {
        return Mono.justOrEmpty(reservationRepository.findById(id))
                .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND, "Reserva não encontrada!")))
                .map(request -> MapperService.mapToReservationEntity(serviceRequestDTO))
                .flatMap(reservationRepository::save)
                .map(MapperService::mapToServiceResponse);
    }

    @Override
    public Mono<Void> delete(String id) {
        return findByIDResponse(id)
                .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND, "Reserva não encontrada!")))
                .flatMap(reservation -> reservationRepository.deleteById(id));
    }

    @Override
    public Mono<Void> deleteMany(List<Reservation> reservations){

      return reservationRepository.deleteAll(reservations);

    }
}
