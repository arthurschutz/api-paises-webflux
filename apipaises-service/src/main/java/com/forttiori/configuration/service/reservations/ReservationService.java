package com.forttiori.configuration.service.reservations;

import com.forttiori.configuration.entity.Reservation;
import com.forttiori.configuration.service.dto.ServiceRequestDTO;
import com.forttiori.configuration.service.dto.ServiceResponseDTO;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;


@Service
public interface ReservationService {

    Mono<ServiceResponseDTO> save(ServiceRequestDTO serviceRequestDTO);
    Flux<Reservation> findAll();
    Mono<Reservation> findByIdEntity (String id);
    Mono<ServiceResponseDTO> findByIDResponse(String id);
    Mono<ServiceResponseDTO> update(String id, ServiceRequestDTO serviceRequestDTO);
    Mono<Void> delete(String id);
    Mono<Void> deleteMany(List<Reservation> reservations);
}
