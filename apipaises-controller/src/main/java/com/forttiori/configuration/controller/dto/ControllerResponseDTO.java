package com.forttiori.configuration.controller.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;


@Data
@Builder
public class ControllerResponseDTO {

    @ApiModelProperty(value = "Código da reserva")
    @Id
    private String id;

    @ApiModelProperty(value = "Nome do país")
    private String name;

    @ApiModelProperty(value = "Região do país")
    private String region;

    @ApiModelProperty(value = "Capital do país")
    private String capital;

}
