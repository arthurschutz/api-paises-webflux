package com.forttiori.configuration.controller;

import com.forttiori.configuration.controller.dto.ControllerRequestDTO;
import com.forttiori.configuration.controller.dto.ControllerResponseDTO;
import com.forttiori.configuration.controller.dto.MapperController;
import com.forttiori.configuration.entity.Reservation;
import com.forttiori.configuration.service.dto.MapperService;
import com.forttiori.configuration.service.dto.ServiceResponseDTO;
import com.forttiori.configuration.service.reservations.ReservationService;
import com.forttiori.configuration.service.reservations.ReservationServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.w3c.dom.stylesheets.LinkStyle;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import javax.validation.Valid;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("api/paises/reservas")
@AllArgsConstructor
@Api("Reservation Resource")
public class ReservationController {

    private final ReservationServiceImpl reservationService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ApiResponse(code = 201, message = "Reserva cadastrada")
    @ApiOperation("Salva uma reserva no banco de dados")
    public Mono<ControllerResponseDTO> save(@RequestBody @Valid ControllerRequestDTO controllerRequestDTO) {
        return Mono.justOrEmpty(controllerRequestDTO)
                .map(MapperController::mapToServiceRequest)
                .flatMap(reservationService::save)
                .map(MapperController::mapToControllerResponse);
    }

    @GetMapping("/reservations")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponse(code = 200, message = "Lista com todas as reservas")
    @ApiOperation("Retorna todas as reservas já feitas")
    public Flux<Reservation> findAll() {
        return reservationService.findAll();
    }

    @GetMapping("/{id}")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna a reserva pelo ID"),
            @ApiResponse(code = 404, message = "Reserva não encontrada")
    })
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation("Retorna a reserva pelo ID")
    public Mono<ControllerResponseDTO> findByID(@PathVariable String id) {
        return reservationService.findByIDResponse(id).map(MapperController::mapToControllerResponse);
    }

    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponse(code = 200, message = "Reserva atualizada")
    @ApiOperation("Atualiza a reserva")
    public Mono<ControllerResponseDTO> update(@PathVariable String id, @RequestBody @Valid ControllerRequestDTO controllerRequestDTO) {
        return Mono.justOrEmpty(controllerRequestDTO)
                .map(MapperController::mapToServiceRequest)
                .flatMap(repository -> reservationService.update(id, MapperController.mapToServiceRequest(controllerRequestDTO)))
                .map(MapperController::mapToControllerResponse);

    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiResponse(code = 204, message = "Reserva deletada pelo ID")
    @ApiOperation("Deleta uma reserva")
    public Mono<Void> delete(@PathVariable String id) {
        return this.reservationService.delete(id);
    }

    @DeleteMapping
    @ResponseStatus(NO_CONTENT)
    @ApiResponse(code = 204, message = "Deleta uma lista de Reservas")
    @ApiOperation("Reservas deletadas pelos ID's")
    public Mono<Void> deleteMany(@RequestBody @Valid List<Reservation> reservations) {
        return reservationService.deleteMany(reservations);
    }

    @ResponseStatus(OK)
    @ApiResponse(code = 200, message = "Teste Assíncrono")
    @ApiOperation("Teste assíncrono")
    @GetMapping(value="/webflux", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Tuple2<Long, Reservation>> getPlaylistByWebflux(){

        System.out.println("---Start get Reservations by WEBFLUX--- " + LocalDateTime.now());
        Flux<Long> interval = Flux.interval(Duration.ofSeconds(10));
        Flux<Reservation> reservationFlux = reservationService.findAll();

        return Flux.zip(interval, reservationFlux);

    }
}
