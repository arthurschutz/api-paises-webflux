package com.forttiori.configuration.controller;

import com.forttiori.configuration.integration.response.PaisInfoResponse;
import com.forttiori.configuration.service.countries.PaisServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Flux;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PaisControllerTest {

    @Mock
    PaisServiceImpl paisService;

    @InjectMocks
    PaisController paisController;

    @Test
    void deveRetorarOPaisPelaCapital() {
        when(paisService.getPaisByCapital(paisInfoResponse.getCapital())).thenReturn(paisInfoResponseFlux);

        var atual = paisController.getPaisByCapital(paisInfoResponse.getCapital());
        var stubExpected = paisInfoResponseFlux;

        assertEquals(stubExpected, atual);
    }

    @Test
    void deveRetorarOPaisPeloNome() {
        when(paisService.getPaisByName(paisInfoResponse.getName())).thenReturn(paisInfoResponseFlux);

        var atual = paisController.getPaisByName(paisInfoResponse.getName());
        var stubExpected = paisInfoResponseFlux;

        assertEquals(stubExpected, atual);
    }

    @Test
    void deveRetorarOPaisPelaRegiao() {
        when(paisService.getPaisByRegiao(paisInfoResponse.getRegion())).thenReturn(paisInfoResponseFlux);

        var atual = paisController.getPaisByRegiao(paisInfoResponse.getRegion());
        var stubExpected = paisInfoResponseFlux;

        assertEquals(stubExpected, atual);
    }

    @Test
    void deveRetorarTodosOsPaises() {
        when(paisService.getTodosPaises()).thenReturn(paisInfoResponseFlux);

        var atual = paisController.getTodosPaises();
        var stubExpected = paisInfoResponseFlux;

        assertEquals(stubExpected, atual);
    }

    PaisInfoResponse paisInfoResponse = PaisInfoResponse.builder()
            .name("Brasil")
            .capital("Brasília")
            .region("América do Sul")
            .build();

    List<PaisInfoResponse> paisInfoResponseList = new ArrayList<>();

    Flux<PaisInfoResponse> paisInfoResponseFlux = Flux.fromIterable(paisInfoResponseList);

}