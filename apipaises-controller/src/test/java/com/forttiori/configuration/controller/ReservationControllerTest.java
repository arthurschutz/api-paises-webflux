package com.forttiori.configuration.controller;

import com.forttiori.configuration.controller.dto.ControllerRequestDTO;
import com.forttiori.configuration.controller.dto.ControllerResponseDTO;
import com.forttiori.configuration.controller.dto.MapperController;
import com.forttiori.configuration.entity.Reservation;
import com.forttiori.configuration.integration.response.PaisInfoResponse;
import com.forttiori.configuration.persistence.repository.ReservationRepository;
import com.forttiori.configuration.service.dto.MapperService;
import com.forttiori.configuration.service.dto.ServiceRequestDTO;
import com.forttiori.configuration.service.dto.ServiceResponseDTO;
import com.forttiori.configuration.service.reservations.ReservationService;
import com.forttiori.configuration.service.reservations.ReservationServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ReservationControllerTest {

//    @Mock
//    ReservationServiceImpl reservationService;
//
//    @InjectMocks
//    ReservationController reservationController;
//
//    @Test
//    void deveSalvarReserva() {
//        when(reservationService.save(serviceRequestDTO)).thenReturn(serviceResponseDTOMono);
//
//        var atual = this.reservationController.save(controllerRequestDTO);
//        var stubExpected = controllerResponseDTOMono;
//
//        assertEquals(stubExpected, atual);
//    }
//
//    @Test
//    void deveFindAllReservas() {
//        when(reservationService.findAll()).thenReturn(reservationFlux);
//
//        var atual = reservationController.findAll();
//        var stubExpected = reservationFlux;
//
//        assertEquals(stubExpected, atual);
//    }
//
//    @Test
//    void deveFindReservaByID() {
//        when(reservationService.findByIDResponse("1")).thenReturn(serviceResponseDTOMono);
//
//        var atual = reservationController.findByID("1");
//        var stubExpected = controllerResponseDTOMono;
//
//        assertEquals(stubExpected, atual);
//    }
//
//    @Test
//    void deveAtualizarReserva() {
//        when(reservationService.update("1", serviceRequestDTO)).thenReturn(serviceResponseDTOMono);
//
//        var atual = reservationController.update("1", controllerRequestDTO);
//        var stubExpected = controllerResponseDTOMono;
//
//        assertEquals(stubExpected, atual);
//    }
//
//    @Test
//    void deveDeletarUmaReserva() {
//        reservationService.delete("1");
//        verify(reservationService, times(1)).delete("1");
//    }
//
//    @Test
//    void deveDeleteManyReservas() {
//
//        reservationService.deleteMany(reservationList);
//        verify(reservationService, times(1)).deleteMany(reservationList);
//    }
//
//    ServiceRequestDTO serviceRequestDTO = ServiceRequestDTO.builder()
//            .name("Brasil")
//            .capital("Brasilia")
//            .region("America do Sul")
//            .build();
//
//    ServiceResponseDTO serviceResponseDTO = ServiceResponseDTO.builder()
//            .id("1")
//            .name("Brasil")
//            .capital("Brasilia")
//            .region("America do Sul")
//            .build();
//
//    Mono<ServiceResponseDTO> serviceResponseDTOMono = Mono.just(serviceResponseDTO);
//
//    ControllerRequestDTO controllerRequestDTO = ControllerRequestDTO.builder()
//            .name("Brasil")
//            .capital("Brasilia")
//            .region("America do Sul")
//            .build();
//
//    ControllerResponseDTO controllerResponseDTO = ControllerResponseDTO.builder()
//            .id("1")
//            .name("Brasil")
//            .capital("Brasilia")
//            .region("America do Sul")
//            .build();
//
//    Mono<ControllerResponseDTO> controllerResponseDTOMono = Mono.justOrEmpty(controllerResponseDTO);
//
//    List<Reservation> reservationList = Arrays.asList(
//            Reservation.builder().id("1").capital("Lisboa").name("Portugal").region("Europa").build(),
//            Reservation.builder().id("1").capital("Londres").name("Inglaterra").region("Europa").build(),
//            Reservation.builder().id("1").capital("Amsterdã").name("Holanda").region("Europa").build());
//
//    Flux<Reservation> reservationFlux = Flux.fromIterable(reservationList);
}