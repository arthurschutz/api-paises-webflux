package com.forttiori.configuration.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Data
@Document(collection = "reservations")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Reservation {

    @ApiModelProperty(value = "Código da reserva")
    @Id
    private String id;

    @ApiModelProperty(value = "Nome do país")
    private String name;

    @ApiModelProperty(value = "Região do país")
    private String region;

    @ApiModelProperty(value = "Capital do país")
    private String capital;

}
