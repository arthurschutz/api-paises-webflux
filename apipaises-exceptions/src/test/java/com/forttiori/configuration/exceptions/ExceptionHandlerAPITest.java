package com.forttiori.configuration.exceptions;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ExceptionHandlerAPI.class})
class ExceptionHandlerAPITest {

    @Autowired
    ExceptionHandlerAPI exceptionHandlerAPI;

    @Test
    void deveRetornarPrettyException() {
        var stubExpected = StandardError.builder()
                .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .build();
        var stubActual = exceptionHandlerAPI.handleGeneralExceptions(new Exception());
        assertEquals(stubExpected,stubActual);
    }

    @Test
    void deveRetornarPrettyRuntimeException() {
        var stubExpected = StandardError.builder()
                .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .message("Um erro inesperado ocorreu")
                .build();
        var stubActual = exceptionHandlerAPI.handleRuntimeExceptions(new RuntimeException());
        assertEquals(stubExpected,stubActual);
    }

    @Test
    void deveRetornarPrettyIoException() {
        var stubExpected = StandardError.builder()
                .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .message("Erro na entrada/saída de dados")
                .build();
        var stubActual = exceptionHandlerAPI.handleIoExceptionExceptions(new IOException());
        assertEquals(stubExpected,stubActual);
    }

    @Test
    void deveRetornarPrettyStatusException() {
        var exception = new ResponseStatusException(HttpStatus.NOT_FOUND, "test");
        var stubExcpected = ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .headers(HttpHeaders.EMPTY)
                .body(StandardError.builder()
                        .status(404)
                        .message(exception.getMessage())
                        .build());
        var stubActual = exceptionHandlerAPI
                .handleResponseStatusException(exception);
        assertEquals(stubExcpected, stubActual);
    }
}